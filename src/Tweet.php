<?php
class Tweet implements JsonSerializable
{
    private $id;
    private $userId;
    private $text;
    private $creationDate;

    public function __construct()
    {
        $this->id = -1;
        $this->userId = "";
        $this->text = "";
        $this->creationDate = "";
    }
    
    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @param string $userId
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;
        return $this;
    }

    /**
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @param string $text
     */
    public function setText($text)
    {
        $this->text = $text;
        return $this;
    }

    /**
     * @return string
     */
    public function getCreationDate()
    {
        return $this->creationDate;
    }

    /**
     * @param string $creationDate
     */
    public function setCreationDate($creationDate)
    {
        $this->creationDate = $creationDate;
        return $this;
    }

    static public function loadTweetById(mysqli $connection, $id)
    {
        $sql = "SELECT * FROM `Tweets` WHERE id=$id";
        $result = $connection->query($sql);
        if($result == true && $result->num_rows == 1)
        {
            $row = $result->fetch_assoc();

            $loadedTweet = new Tweet();
            $loadedTweet->id = $row['id'];
            $loadedTweet->userId = $row['user_id'];
            $loadedTweet->text = $row['tweet_text'];
            $loadedTweet->creationDate = $row['creation_date'];
            return $loadedTweet;
        }
        return null;
    }

    static public function loadTweetByUserId(mysqli $connection, $userId)
    {
        $sql = "SELECT * FROM `Tweets` WHERE user_id=$userId ORDER BY creation_date DESC";
        $ret = [];
        $result = $connection->query($sql);
        if($result == true && $result->num_rows != 0)
        {
            foreach($result as $row)
            {
                $loadedTweet = new Tweet();
                $loadedTweet->id = $row['id'];
                $loadedTweet->userId = $row['user_id'];
                $loadedTweet->text = $row['tweet_text'];
                $loadedTweet->creationDate = $row['creation_date'];
                $ret[] = $loadedTweet;
            }
        }
        return $ret;
    }

    static public function loadAllTweets(mysqli $connection)
    {
        $sql = "SELECT * FROM `Tweets` ORDER BY creation_date DESC";
        $ret = [];
        $result = $connection->query($sql);
        if($result == true && $result->num_rows != 0)
        {
            foreach($result as $row)
            {
                $loadedTweet = new Tweet();
                $loadedTweet->id = $row['id'];
                $loadedTweet->userId = $row['user_id'];
                $loadedTweet->text = $row['tweet_text'];
                $loadedTweet->creationDate = $row['creation_date'];
                $ret[] = $loadedTweet;
            }
        }
        return $ret;
    }

    public function saveToDB(mysqli $connection)
    {
        if ($this->id == -1)
        {
            //Saving new tweet to DB
            $sql = "INSERT INTO `Tweets`(user_id, tweet_text, creation_date)
                    VALUES ($this->userId,'$this->text','$this->creationDate')";
            $result = $connection->query($sql);
            if($result == true)
            {
                $this->id = $connection->insert_id;
                return true;
            }
        }else
        {
            //Updating tweet
            $sql = "UPDATE `Tweet` SET tweet_text ='$this->text',
                                       creation_date ='$this->creationDate'
                                       WHERE id=$this->id";
            $result = $connection->query($sql);
            if($result == true)
            {
                return true;
            }
        }
        return false;
    }

    public function jsonSerialize() {
        return [
            'id' => $this->id,
            'userId' => $this->userId,
            'text' => $this->text,
            'creationDate' => $this->creationDate
        ];
    }

}