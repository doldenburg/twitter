<?php

class User implements JsonSerializable{
    
    private $id;
    private $username;
    private $fullname;
    private $hashedPassword;
    private $email;

    public function __construct()
    {
        $this->id = -1;
        $this->username = "";
        $this->fullname = "";
        $this->email = "";
        $this->hashedPassword = "";
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param string $username
     */
    public function setUsername($username)
    {
        $this->username = $username;
        return $this;
    }
    
    /**
     * @return string
     */
    public function getFullname()
    {
        return $this->fullname;
    }

    /**
     * @param string $username
     */
    public function setFullname($fullname)
    {
        $this->fullname = $fullname;
        return $this;
    }

    /**
     * @return string
     */
    public function getHashedPassword()
    {
        return $this->hashedPassword;
    }

    /**
     * @param string $newHashedPassword
     */
    public function setPassword($newPassword)
    {
        $newHashedPassword = password_hash($newPassword, PASSWORD_BCRYPT);
        $this->hashedPassword = $newHashedPassword;
        return $this;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }

    public function verifyPassword($password)
    {
        return password_verify($password, $this->hashedPassword);
    }

    public function saveToDB(mysqli $connection)
    {
        if ($this->id == -1)
        {
            //Saving new user to DB
            $sql = "INSERT INTO `Users`(username, fullname, email, hashed_password) "
                    . "VALUES ('$this->username', '$this->fullname', '$this->email','$this->hashedPassword')";
            $result = $connection->query($sql);
            if($result == true)
            {
                $this->id = $connection->insert_id;
                return true;
            }
        }else
        {
            //Updating user
            $sql = "UPDATE `Users` SET username='$this->username',
                                       fullname='$this->fullname',
                                       email='$this->email',
                                       hashed_password='$this->hashedPassword'
                                       WHERE id=$this->id";
            $result = $connection->query($sql);
            if($result == true)
            {
                return true;
            }
        }
        return false;
    }

    static public function loadUserById(mysqli $connection, $id)
    {
        $sql = "SELECT * FROM `Users` WHERE id=$id";
        $result = $connection->query($sql);
        if ($result == true && $result->num_rows == 1)
        {
            $row = $result->fetch_assoc();

            $loadedUser = new User();
            $loadedUser->id = $row['id'];
            $loadedUser->username = $row['username'];
            $loadedUser->fullname = $row['fullname'];
            $loadedUser->email = $row['email'];
            $loadedUser->hashedPassword = $row['hashed_password'];
            return $loadedUser;
        }
        return null;
    }
    
     static public function loadUserByUsername(mysqli $connection, $usernameOrEmail)
    {
        $sql = "SELECT * FROM `Users` WHERE username='$usernameOrEmail' OR email='$usernameOrEmail'";
        $result = $connection->query($sql);
        if ($result == true && $result->num_rows == 1)
        {
            $row = $result->fetch_assoc();

            $loadedUser = new User();
            $loadedUser->id = $row['id'];
            $loadedUser->username = $row['username'];
            $loadedUser->fullname = $row['fullname'];
            $loadedUser->email = $row['email'];
            $loadedUser->hashedPassword = $row['hashed_password'];
            return $loadedUser;
        }
        return null;
    }

    static public function loadAllUsers(mysqli $connection)
    {
        $sql = "SELECT * FROM `Users`";
        $ret = [];
        $result = $connection->query($sql);
        if($result == true && $result->num_rows != 0)
        {
            foreach($result as $row)
            {
                $loadedUser = new User();
                $loadedUser->id = $row['id'];
                $loadedUser->username = $row['username'];
                $loadedUser->fullname = $row['fullname'];
                $loadedUser->email = $row['email'];
                $loadedUser->hashedPassword = $row['hashed_password'];
                $ret[] = $loadedUser;
            }
        }
        return $ret;
    }

    public function delete(mysqli $connection)
    {
        if($this->id != -1)
        {
            $sql = "DELETE FROM `Users` WHERE id=$this->id";
            $result = $connection->query($sql);
            if($result == true)
            {
                $this->id = -1;
                return true;
            }
            return false;
        }
        return true;
    }

    public static function logIn(mysqli $connection, $email, $password)
    {
//        $email = $connection->real_escape_string($email);
        $sql = "SELECT * FROM `Users` WHERE email='$email'";
//        $sql = sprintf("SELECT * FROM `Users` WHERE email='%s'",
//                        $connection->real_escape_string($email));
        $result = $connection->query($sql);
        echo $result->num_rows;
        
        if(($result == true) && ($result->num_rows == 1))
        {
            $row = $result->fetch_assoc();
            if(password_verify($password, $row['hashed_password'])) {
                $loadedUser = new User();
                $loadedUser->id = $row['id'];
                $loadedUser->username = $row['username'];
                $loadedUser->fullname = $row['fullname'];
                $loadedUser->email = $row['email'];
                $loadedUser->hashedPassword = $row['hashed_password'];

                return $loadedUser;
            }else
            {
                return false;
            }
        }else
        {
            return false;
        }
    }
    
    public function jsonSerialize() {
        return [
            'id' => $this->id,
            'username' => $this->username,
            'fullname' => $this->fullname,
            'email' => $this->email
        ];
    }

}
