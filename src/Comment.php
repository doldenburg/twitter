<?php

class Comment implements JsonSerializable
{
    private $id;
    private $userId;
    private $postId;
    private $creationDate;
    private $text;
    
    public function __construct() 
    {
        $this->id = -1;
        $this->userId = "";
        $this->postId = "";
        $this->creationDate = "";
        $this->text = "";
    }
    
    public function getId() 
    {
        return $this->id;
    }

    public function getUserId() 
    {
        return $this->userId;
    }
    
    public function setUserId($userId) 
    {
        $this->userId = $userId;
        return $this;
    }

    public function getPostId() 
    {
        return $this->postId;
    }
    
    public function setPostId($postId) 
    {
        $this->postId = $postId;
        return $this;
    }

    public function getCreationDate() 
    {
        return $this->creationDate;
    }
    
    public function setCreationDate($creationDate) 
    {
        $this->creationDate = $creationDate;
        return $this;
    }

    public function getText() 
    {
        return $this->text;
    }   

    public function setText($text) 
    {
        $this->text = $text;
        return $this;
    }
    
    static public function loadCommentById(mysqli $connection, $id)
    {
        $sql = "SELECT * FROM `Comments` WHERE id=$id";
        $result = $connection->query($sql);
        if($result == true && $result->num_rows == 1)
        {
            $row = $result->fetch_assoc();
            
            $loadedComment = new Comment();
            $loadedComment->id = $row['id'];
            $loadedComment->userId = $row['user_id'];
            $loadedComment->postId = $row['post_id'];
            $loadedComment->creationDate = $row['creation_date'];
            $loadedComment->text = $row['text'];
            
            return $loadedComment;
        }
        return null;
    }
    
    static public function loadAllCommentsByPostId(mysqli $connection, $id)
    {
        $sql = "SELECT * FROM `Comments` WHERE post_id=$id ORDER BY creation_date DESC";
        $ret = [];
        $result = $connection->query($sql);
        if($result == true && $result->num_rows != 0)
        {
            foreach($result as $row)
            {
                $loadedComment = new Comment();
                $loadedComment->id = $row['id'];
                $loadedComment->userId = $row['user_id'];
                $loadedComment->postId = $row['post_id'];
                $loadedComment->creationDate = $row['creation_date'];
                $loadedComment->text = $row['text'];
                $ret[] = $loadedComment;
            }
        }
        return $ret;
    }
    
    public function saveToDB($connection)
    {
        if($this->id == -1)
        {
            $sql = "INSERT INTO `Comments` (user_id, post_id, creation_date, text) "
                    . "VALUES ($this->userId, $this->postId, '$this->creationDate', '$this->text')";
            $result = $connection->query($sql);
            if($result == true)
            {
                $this->id = $connection->insert_id;
                return true;
            }
        }else
        {
            
        }
        return false;
    }

    public function jsonSerialize() {
        return [
            'id' => $this->id,
            'userId' => $this->userId,
            'postId' => $this->postId,
            'creationDate' => $this->creationDate,
            'text' => $this->text
        ];
    }
    
}
