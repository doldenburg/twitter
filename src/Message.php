<?php

class Message implements JsonSerializable
{
    private $id;
    private $senderId;
    private $recipientId;
    private $ifRead;
    private $creationDate;
    private $content;
    
    public function __construct() {
        $this->id = -1;
        $this->senderId = "";
        $this->recipientId = "";
        $this->ifRead = "";
        $this->creationDate = "";
        $this->content = "";
    }
    
    public function getId() {
        return $this->id;
    }

    public function getSenderId() {
        return $this->senderId;
    }
    
    public function setSenderId($senderId) {
        $this->senderId = $senderId;
        return $this;
    }

    public function getRecipientId() {
        return $this->recipientId;
    }
    
    public function setRecipientId($recipientId) {
        $this->recipientId = $recipientId;
        return $this;
    }

    public function getIfRead() {
        return $this->ifRead;
    }
    
    public function setIfRead($ifRead) {
        $this->ifRead = $ifRead;
        return $this;
    }

    public function getCreationDate() {
        return $this->creationDate;
    }
    
    public function setCreationDate($creationDate) {
        $this->creationDate = $creationDate;
        return $this;
    }

    public function getContent() {
        return $this->content;
    }
    
    public function setContent($content) {
        $this->content = $content;
        return $this;
    }
    
    static public function loadAllMessagesByUserId(mysqli $connection, $id)
    {
        $sql = "SELECT * FROM `Messages` WHERE sender_id=$id OR recipient_id=$id ORDER BY creation_date DESC";
        $ret = [];
        $result = $connection->query($sql);
        if($result == true && $result->num_rows != 0)
        {
            foreach($result as $row)
            {
                $loadedMessage = new Message();
                $loadedMessage->id = $row['id'];
                $loadedMessage->senderId = $row['sender_id'];
                $loadedMessage->recipientId = $row['recipient_id'];
                $loadedMessage->creationDate = $row['creation_date'];
                $loadedMessage->content = $row['content'];
                $ret[] = $loadedMessage;
            }
        }
        return $ret;
    }
    
    static public function loadAllReceivedMessages(mysqli $connection, $id)
    {
        $sql = "SELECT * FROM `Messages` WHERE recipient_id=$id ORDER BY creation_date DESC";
        $receivedMessages = [];
        $result = $connection->query($sql);
        if($result == true && $result->num_rows != 0)
        {
            foreach($result as $row)
            {
                $loadedMessage = new Message();
                $loadedMessage->id = $row['id'];
                $loadedMessage->senderId = $row['sender_id'];
                $loadedMessage->recipientId = $row['recipient_id'];
                $loadedMessage->creationDate = $row['creation_date'];
                $loadedMessage->content = $row['content'];
                $receivedMessages[] = $loadedMessage;
            }
        }
        return $receivedMessages;
    }
    
        static public function loadAllSendMessages(mysqli $connection, $id)
    {
        $sql = "SELECT * FROM `Messages` WHERE sender_id=$id ORDER BY creation_date DESC";
        $senderMessages = [];
        $result = $connection->query($sql);
        if($result == true && $result->num_rows != 0)
        {
            foreach($result as $row)
            {
                $loadedMessage = new Message();
                $loadedMessage->id = $row['id'];
                $loadedMessage->senderId = $row['sender_id'];
                $loadedMessage->recipientId = $row['recipient_id'];
                $loadedMessage->creationDate = $row['creation_date'];
                $loadedMessage->content = $row['content'];
                $senderMessages[] = $loadedMessage;
            }
        }
        return $senderMessages;
    }
    
    static public function loadMessageById(mysqli $connection, $id)
    {
        $sql = "SELECT * FROM `Messages` WHERE id=$id";
        $result = $connection->query($sql);
        if($result == true && $result->num_rows == 1)
        {
            $row = $result->fetch_assoc();
            
            $loadedMessage = new Message();
            $loadedMessage->id = $row['id'];
            $loadedMessage->senderId = $row['sender_id'];
            $loadedMessage->recipientId = $row['recipient_id'];
            $loadedMessage->creationDate = $row['creation_date'];
            $loadedMessage->content = $row['content'];
            return $loadedMessage;    
        }
        return null;
    }
    
    public function saveToDB($connection)
    {
        if($this->id == -1)
        {
            $sql = "INSERT INTO `Messages` (sender_id, recipient_id, if_read, creation_date, content) "
                    . "VALUES ($this->senderId, $this->recipientId, $this->ifRead,'$this->creationDate', '$this->content')";
            $result = $connection->query($sql);
            if($result == true)
            {
                $this->id = $connection->insert_id;
                return true;
            }
        }else
        {
            
        }
        return false;
    }
    
    public function jsonSerialize() {
        return [
            'id' => $this->id,
            'senderId' => $this->senderId,
            'recipientId' => $this->recipientId,
            'creationDate' => $this->creationDate,
            'content' => $this->content
        ];
    }
  
}

