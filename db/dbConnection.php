<?php
require_once "dbConfig.php";

$conn = new mysqli($servername, $username, $password, $dbname);
$conn->set_charset("utf8");
if ($conn->connect_error)
{
    die("Connection failed. Error: ".$conn->connect_error);
}
