<?php
session_start();
include_once 'library.php';


if(!isset($_SESSION['loggedUserId'])){
    header("Location: logIn.php");
    die();
}

if($_SERVER['REQUEST_METHOD'] === 'GET'){
    if(isset($_GET['userId']) && !empty($_GET['userId'])){
        $profileId = $_GET['userId'];
        $profile = User::loadUserById($conn, $profileId);
        $profileFullname = $profile->getFullname();
        $profileUsername = $profile->getUsername();
    } else {
        header("Location: index.php");
        die();
    }
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Twitter</title>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="../css/myStyle.css">
    
    <link rel="apple-touch-icon" sizes="76x76" href="../favicons/apple-touch-icon.png">
    <link rel="icon" type="image/png" href="../favicons/favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="../favicons/favicon-16x16.png" sizes="16x16">
    <link rel="manifest" href="../favicons/manifest.json">
    <link rel="mask-icon" href="../favicons/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="theme-color" content="#ffffff">
</head>
<body>
    <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
        <div class="container">    
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="navcol-1">
                <a class="navbar-brand navbar-center">
                    <span class="glyphicon glyphicon-flash logo" aria-hidden="true"></span>
                </a>
                
                <ul class="nav navbar-nav navbar-left">
                    <li class="active"><a href="index.php"><span class="glyphicon glyphicon-home" aria-hidden="true"></span> Home</a></li>
                    <li><a href="messagesPage.php"><span class="glyphicon glyphicon-envelope" aria-hidden="true"></span> Messages</a></li>
                </ul>   
                
                <ul class="nav navbar-nav navbar-right">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle my-tooltip my-dropdown" data-toggle="dropdown" data-trigger="hover" data-placement="bottom" title="Profile and settings"><span class="glyphicon glyphicon-cog" aria-hidden="true"></span> <span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="userPage.php?userId=<?php echo $_SESSION['loggedUserId']?>"><?php echo "<b>" . $_SESSION['fullname'] . "</b>";?><br>View profile</a></li>
                            <li class="divider" role="separator"></li>
                            <li><a href="editUser.php">Settings</a></li>
                            <li><a href="logOut.php">Log out</a></li>
                        </ul>
                    </li>
                    <li><button type="button" class="btn btn-primary navbar-btn" data-toggle="modal" data-target="#myModal">Tweet</button></li>
                </ul>

                <form class="navbar-form navbar-right" role="search">
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="Search Twitter">
                        <span class="input-group-btn">
                            <button type="button" class="btn btn-default">
                                <i class="glyphicon glyphicon-search"></i>
                            </button>
                        </span>
                    </div>
                </form>
            </div><!-- /.navbar-collapse -->
        </div>
    </nav>
    <div class="container">
        <div class="row">
            <div class="col-lg-3">
                <div class="panel panel-default profile">
                    <div class="avatar">
                        <span class="glyphicon glyphicon-camera dziecko icon" aria-hidden="true">   
                        </span>
                    </div>
                    <div class="panel-heading profile-background">
                    </div>
                    <div class="panel-body profile-card">
                        <div class="row">
                            <div class="col-lg-3">

                            </div>
                            <div class="col-lg-offset-1 col-lg-8">
                                <?php
                                echo '<strong><a href="userPage.php?userId='.$profileId.'" class="link-username fullname">'.$profileFullname.'</a></strong>'.
                                     '<p><small><a href="userPage.php?userId='.$profileId.'" class="link-username">@<span id="username">'.$profileUsername.'</span></a></small></p>';
                                ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-3">
                                <small>TWEETS</small>
                                <?php
                                $countTweet = count(Tweet::loadTweetByUserId($conn, $profileId));
                                echo '<span class="profile-card-stats">'.$countTweet.'</span>';
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
                <?php
                if($_SESSION['loggedUserId'] != $profileId){
                    echo '<button type="submit" class="btn btn-warning btn-block send-message" data-toggle="modal" data-target="#myNewMessage">Send message</button>';
                }
                ?>
            </div>
            
            <div class="col-lg-6">
                <div class="panel panel-default panels">
                    <?php
                    $search = Tweet::loadTweetByUserId($conn, $profileId);
                    if (count($search) === 0){
                        echo '<div class="panel-heading">
                                <form method="POST" action="index.php">
                                    <div class="form-group">
                                        <p><h4>Write your first Tweet!</h4></p>
                                        <textarea name="tweet" maxlength="140" class="form-control" placeholder="What\'s happening?"></textarea>
                                    </div>
                                    <button type="submit" class="btn btn-primary pull-right">Tweet</button>
                                </form>
                             </div>';
                    }
                    ?>
                    <div class = "panel-body">
                        <div class="list-group">
                            <?php
                            foreach($search as $row)
                            {
                                $user = User::loadUserById($conn, $row->getUserId());
                                echo '<div class="list-group-item link" data-toggle="modal" data-target="#myPost" data-id="'.$row->getId().'">'.
                                        '<div class="inline-group">'.
                                        '<strong><a href="userPage.php?userId='.$user->getId().'" class="link-username">'.$user->getFullname().'</a></strong> <small> @'.$user->getUsername().' </small> &sdot; <small>'.date("d M Y", strtotime($row->getCreationDate())).'</small>'.
                                        '</div>'.
                                        '<p class="list-group-item-text">'.
                                            $row->getText().
                                        '</p>'.
                                    '</div>';
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <!-- Modal -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Compose new Tweet</h4>
                </div>
                <div class="modal-body">
                    <form method="POST" action="index.php">
                        <div class="form-group">
                            <textarea name="tweet" maxlength="140" class="form-control" placeholder="What's happening?"></textarea>
                        </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary pull-right">Tweet</button>
                </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    
    <!-- Modal -->
    <div class="modal fade" id="myPost" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <div id="result"></div>    
                    <!--<h4 class="modal-title" id="myModalLabel">Compose new Tweet</h4>-->

                </div>
                <div class="modal-body">
                    <form method="POST" action="index.php" id="formComment">
                        <div class="form-group">
                            <input type="hidden" name="tweetId" value="">
                            <textarea name="comment" rows="2" class="form-control" placeholder="Write comment..."></textarea>
                        </div>
                
                    <button type="submit" class="btn btn-primary pull-right">Comment</button>
                    </form>
                </div>
                <div class="modal-header comments"></div>
            </div>
        </div>
    </div>
    
    <!-- Modal -->
    <div class="modal fade" id="myNewMessage" tabindex="-1" role="dialog" aria-labelledby="myNewMessageLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myNewMessageLabel">New message</h4>
                </div>
                <div class="modal-body">
                    <form method="POST" action="messagesPage.php">
                        <div class="form-group">
                            <label for="recipientLabel">Recipient:</label>
                            <input name="recipient" type="text" class="form-control" id="recipientLabel" placeholder="Recipient">
                        </div>    
                        <div class="form-group">
                                <label for="messageContent">Message content:</label>
                                <textarea name="messageContent" class="form-control" id="messageContent" rows="5"></textarea>
                        </div>
                        <div class="modal-footer">
                            <div class="form-inline pull-right">
                            
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>  
                            <button type="submit" class="btn btn-primary">Send</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <script>
        $('.my-dropdown').tooltip({
            animation: true,
            trigger: 'hover'
        });        
        
        var commentDiv = $('#myPost').find('.comments');    
        
        $('.send-message').on('click', function() {
            var username = $('#username').text();
            var inputRecipient = $("input[name='recipient']");
            inputRecipient.attr('value', username);
        });  
        
        $('.link').on('click', function() {
            var numberTweet = $(this).data("id");
            var desc = $('#result');
            var inputId = $("input[name='tweetId']").val(numberTweet);

            $.ajax({
                url: 'tweetAPI.php',
                type: 'GET',
                data: {id: numberTweet},
                dataType: 'json'
            })
            .done(function(result) {
                var tweet = result.tweet;
                var user = result.user;
                var comment = result.comment;
                var usersComment = result.usersComment;
  
                desc.html(
                        '<p><strong><a href="userPage.php?userId='+ user.id +'" class="link-username">' + user.fullname + '</a></strong> &sdot; @' + user.username + '</p>'+
                        '<p>' + tweet.text + '</p>');
                
                commentDiv.empty();
                $.each(comment, function(i, item) {
                    commentDiv.append(
                        '<p>' + item.postId + '</p>'+
                        '<p>' + item.text + '</p>');  
                    
                })

            })
            .fail(function(request, status, error){
                console.log(status, error);
            });

        });     
        
        
        $("#formComment").submit(function(e){
            e.preventDefault();
            var input = $(this).find('textarea');
           
            $.ajax({
                url: 'tweetAPI.php',
                type: 'post',
                dataType: 'json',
                data: $(this).serialize(),
            }).done(function(result){
                var comment = result.comment;
                var user = result.user;
                
                input.val('');
                console.log(user.username);
                commentDiv.prepend(
                                    '<p>' + user.username + '</p>'+
                                    '<p>' + comment.text + '</p>'); 
            })
               
        });
    </script>    
</body>
</html>
