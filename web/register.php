<?php
session_start();
require_once 'library.php';

if(isset($_SESSION['loggedUserId'])){
    header("Location: index.php");
    die();
}

if($_SERVER['REQUEST_METHOD'] === 'POST')
{
    unset($_SESSION['e_fullname']);
    unset($_SESSION['e_username']);
    unset($_SESSION['e_email']);
    unset($_SESSION['e_password']);
    if(isset($_POST['fullname']) && isset($_POST['username']) && isset($_POST['email']) && isset($_POST['password']) &&
       !empty($_POST['fullname']) && !empty($_POST['username']) && !empty($_POST['email']) && !empty($_POST['password']))
    {
        $status = true;
        
        $fullname = $conn->real_escape_string($_POST['fullname']);
        $fullname = htmlspecialchars(trim($fullname));

        $username = $conn->real_escape_string($_POST['username']);
        $username = htmlspecialchars(trim($username));
        
        $checkUsername = User::loadUserByUsername($conn, $username);
        if(isset($checkUsername)){
            $status = false;
            $_SESSION['e_username'] = "Given username is taken.";
        }
        
	if ((strlen($username) < 3) || (strlen($username) > 20))
	{
            $status = false;
            $_SESSION['e_username'] = "Username must have between 3 and 20 characters.";
        }
		
	if (ctype_alnum($username) == false)
	{
            $status = false;
            $_SESSION['e_username'] = "Username can only consist of letters and numbers (without polish characters).";
	}
        
        $email = trim($_POST['email']);
        $email = filter_var($email, FILTER_SANITIZE_EMAIL);
        if ((filter_var($email, FILTER_VALIDATE_EMAIL) == false))
        {
            $status = false;
            $_SESSION['e_email'] = "Please enter a valid email address.";
        }
        
        $checkEmail = User::loadUserByUsername($conn, $email);
        if(isset($checkEmail)){
            $status = false;
            $_SESSION['e_email'] = "Given email address is taken.";
        }
        
        $password = trim($_POST['password']);
        
        if ((strlen($password) < 8) || (strlen($password) > 20))
	{
            $status = false;
            $_SESSION['e_password'] = "Password must have between 8 and 20 characters.";
	}
        		
        if($status === true){

            $loggedUser = new User();
            $loggedUser->setFullname($fullname)->setUsername($username)->setEmail($email)->setPassword($password);
            $result = $loggedUser->saveToDB($conn);

            if($result)
            {
                $_SESSION['loggedUserId'] = $loggedUser->getId();
                $_SESSION['fullname'] = $loggedUser->getFullname();
                $_SESSION['username'] = $loggedUser->getUsername();
                if(isset($_SESSION['registerError']))
                {
                    unset($_SESSION['registerError']);
                    unset($_SESSION['e_fullname']);
                    unset($_SESSION['e_username']);
                    unset($_SESSION['e_email']);
                    unset($_SESSION['e_password']);
                    unset($_SESSION['f_fullname']);
                    unset($_SESSION['f_username']);
                    unset($_SESSION['f_email']);
                    unset($_SESSION['f_password']);
                }
                header('Location: index.php');
                die();
            }else { 
                $_SESSION['registerError'] = "Failed to register."; 
                header('Location: register.php');
            }      
        } else {
            $_SESSION['f_fullname'] = $fullname;
            $_SESSION['f_username'] = $username;
            $_SESSION['f_email'] = $email;
            $_SESSION['f_password'] = $password;
            $_SESSION['registerError'] = "Failed to register. Incorrect fullname, username, e-mail or password."; 
            header('Location: register.php');
        }
    }else
    {
        unset($_SESSION['f_username']);
        if(isset($_POST['fullname'])){
        $_SESSION['f_fullname'] = $_POST['fullname'];
        }
        if(isset($_POST['username'])){
        $_SESSION['f_username'] = $_POST['username'];
        }
        if(isset($_POST['email'])){
        $_SESSION['f_email'] = $_POST['email'];
        }
        if(isset($_POST['password'])){
        $_SESSION['f_password'] = $_POST['password'];
        }
        $_SESSION['registerError'] = "Failed to register. Fill in all fields.";
        header('Location: register.php');
    }
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Twitter</title>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="../css/myStyle.css">
</head>
<body>
     <nav class="navbar navbar-default navbar-fixed-top grad" role="navigation">
        <div class="container">    
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="navcol-1">
                <a class="navbar-brand navbar-center">
                    <span class="glyphicon glyphicon-flash register-color" aria-hidden="true"></span>
                </a> 
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="logIn.php" class="register-color">Have an account? Log in</a></li>
                </ul>
            </div><!-- /.navbar-collapse -->
        </div>
    </nav>
    
    <div class="register">
        <div class="container">
        <!-- Marketing Icons Section -->
            <div class="row">
                <div class="col-md-offset-4 col-md-4">
                            
                            <form method="POST" action="register.php" class="signUpForm">
                                <?php
                                if(isset($_SESSION['registerError'])){
                                    echo "<div class=\"alert alert-warning\">"
                                          ."<a href=\"#\" class=\"close\" data-dismiss=\"alert\">&times;</a>"
                                          ."<strong>Warning! </strong>"
                                          .$_SESSION['registerError']
                                          ."</div>";
                                }
                                ?>
                                <div class="form-group row">
                                    <div class="col-md-12">
                                        <h2>Join Twitter today.</h2>
                                    </div>    
                                </div>
                                <div class="form-group row">
                                    <div class="col-md-12">
                                        <input type="text" name="fullname" class="form-control" placeholder="Full name" <?php
                                        if(isset($_SESSION['f_fullname'])){
                                            echo 'value="'.$_SESSION['f_fullname'].'"';
                                        }?>>  
                                    </div>   
                                    <?php 
                                        if(isset($_SESSION['e_fullname'])){
                                            echo '<div class="col-md-12">'.
                                                 '<div class="alert alert-danger">'.
                                                  $_SESSION['e_fullname'].
                                                 '</div>'.
                                                 '</div>';
                                        }
                                    ?>
                                </div>    
                                <div class="form-group row">
                                    <div class="col-md-12">
                                        <input type="text" name="username" class="form-control" placeholder="Username" <?php
                                        if(isset($_SESSION['f_username'])){
                                            echo 'value="'.$_SESSION['f_username'].'"';
                                        }?>>
                                    </div>
                                    <?php 
                                        if(isset($_SESSION['e_username'])){
                                            echo '<div class="col-md-12">'.
                                                 '<div class="alert alert-danger">'.
                                                  $_SESSION['e_username'].
                                                 '</div>'.
                                                 '</div>';
                                        }
                                    ?>
                                    
                                </div>  
                                <div class="form-group row">    
                                    <div class="col-md-12">
                                        <input type="email" name="email" class="form-control" placeholder="Email" <?php
                                        if(isset($_SESSION['f_email'])){
                                            echo 'value="'.$_SESSION['f_email'].'"';
                                        }?>>
                                    </div>
                                    <?php 
                                        if(isset($_SESSION['e_email'])){
                                            echo '<div class="col-md-12">'.
                                                 '<div class="alert alert-danger">'.
                                                  $_SESSION['e_email'].
                                                 '</div>'.
                                                 '</div>';
                                        }
                                    ?>
                                </div>
                                <div class="form-group row">    
                                    <div class="col-md-12">
                                        <input type="password" name="password" class="form-control" placeholder="Password" <?php
                                        if(isset($_SESSION['f_password'])){
                                            echo 'value="'.$_SESSION['f_password'].'"';
                                        }?>>  
                                    </div>
                                    <?php 
                                        if(isset($_SESSION['e_password'])){
                                            echo '<div class="col-md-12">'.
                                                 '<div class="alert alert-danger">'.
                                                  $_SESSION['e_password'].
                                                 '</div>'.
                                                 '</div>';
                                        }
                                    ?>
                                </div>
                                <div class="form-group row"> 
                                    <div class="col-md-12">
                                    <button type="submit" class="btn btn-primary btn-lg btn-block">Sign up</button>
                                    </div>
                                </div>
                                <div class="form-group row"> 
                                    <div class="col-md-12 signUp">
                                    By signing up, you agree to the Terms of Service and Privacy Policy, including Cookie Use. Others will be able to find you by email or phone number when provided.
                                    </div>
                                </div>
                            </form>
                </div>
            </div> 
        </div>
    </div>
    
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <script>
    </script>    
</body>
</html>

