<?php
session_start();
require 'library.php';

header('Content-Type: application/json');

if($_SERVER['REQUEST_METHOD'] === 'GET'){
    $tweet = [];
    if(isset($_GET['id']) && intval($_GET['id'])>0)
    {
        $tweet = Tweet::loadTweetById($conn, intval($_GET['id']));
        $user = User::loadUserById($conn, $tweet->getUserId());
        $comments = Comment::loadAllCommentsByPostId($conn, $_GET['id']);
        $table = [];
        foreach($comments as $comment){
            $userComment = User::loadUserById($conn, $comment->getUserId());
            $table2 = (array("comment" => $comment, "userComment" => $userComment));
            array_push($table, $table2);
        }
        echo json_encode(array("tweet" => $tweet, "user" => $user, "comment" => $comments, "table" => $table));
    }

} elseif ($_SERVER['REQUEST_METHOD'] === 'POST'){
    if(isset($_POST['comment']) && $_POST['comment'] != ""&&
       isset($_POST['tweetId']) && intval($_POST['tweetId']) > 0)
    {   
        $comment = new Comment();
        $comment->setPostId($_POST['tweetId']);
        $comment->setUserId($_SESSION['loggedUserId']);
        $comment->setCreationDate(date('Y-m-d G-i-s'));
        $comment->setText($_POST['comment']);
        
        $comment->saveToDB($conn);
        $user = User::loadUserById($conn, $_SESSION['loggedUserId']);
        
        echo json_encode(array("comment" => $comment, "user" => $user));
//        echo json_encode($comment);

        
    }
}

?>

