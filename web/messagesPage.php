<?php
session_start();
include_once 'library.php';


if(!isset($_SESSION['loggedUserId']))
{
    header("Location: logIn.php");
    die();
}

if($_SERVER['REQUEST_METHOD'] === 'POST')
{
    if(isset($_POST['tweet']) && $_POST['tweet'] != "")
    {
        
        $addTweet = trim($_POST['tweet']);
        
        $tweet = new Tweet();
        $tweet->setUserId($_SESSION['loggedUserId'])->setText($addTweet)->setCreationDate(date('Y-m-d G-i-s'));
        $tweet->saveToDB($conn);
    }
    if(isset($_POST['recipient']) && $_POST['recipient'] != "")
    {
        if(isset($_POST['messageContent']) && $_POST['messageContent'] != "")
        {
            $recipient = $conn->real_escape_string($_POST['recipient']);
            $recipient = htmlspecialchars(trim($recipient));
            
            $messageContent = $conn->real_escape_string($_POST['messageContent']);
            $messageContent = htmlspecialchars(trim($messageContent));
            
            $recipientFromDB = User::loadUserByUsername($conn, $recipient);

            $newMessage = new Message();
            $newMessage->setSenderId($_SESSION['loggedUserId']);
            $newMessage->setRecipientId($recipientFromDB->getId());
            $newMessage->setIfRead(1);
            $newMessage->setCreationDate(date('Y-m-d G-i-s'));
            $newMessage->setContent($messageContent);
            
            $newMessage->saveToDB($conn);
        }
    }
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Twitter</title>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="../css/myStyle.css">
    
    <link rel="apple-touch-icon" sizes="76x76" href="../favicons/apple-touch-icon.png">
    <link rel="icon" type="image/png" href="../favicons/favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="../favicons/favicon-16x16.png" sizes="16x16">
    <link rel="manifest" href="../favicons/manifest.json">
    <link rel="mask-icon" href="../favicons/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="theme-color" content="#ffffff">
</head>
<body>
    <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
        <div class="container">    
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="navcol-1">
                <a class="navbar-brand navbar-center">
                    <span class="glyphicon glyphicon-flash logo" aria-hidden="true"></span>
                </a>
                
                <ul class="nav navbar-nav navbar-left">
                    <li><a href="index.php"><span class="glyphicon glyphicon-home" aria-hidden="true"></span> Home</a></li>
                    <li class="active"><a href="messagesPage.php"><span class="glyphicon glyphicon-envelope" aria-hidden="true"></span> Messages</a></li>
                </ul>   
                
                <ul class="nav navbar-nav navbar-right">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle my-tooltip my-dropdown" data-toggle="dropdown" data-trigger="hover" data-placement="bottom" title="Profile and settings"><span class="glyphicon glyphicon-cog" aria-hidden="true"></span> <span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="userPage.php?userId=<?php echo $_SESSION['loggedUserId']?>"><?php echo "<b>" . $_SESSION['fullname'] . "</b>";?><br>View profile</a></li>
                            <li class="divider" role="separator"></li>
                            <li><a href="editUser.php">Settings</a></li>
                            <li><a href="logOut.php">Log out</a></li>
                        </ul>
                    </li>
                    <li><button type="button" class="btn btn-primary navbar-btn" data-toggle="modal" data-target="#myModal">Tweet</button></li>
                </ul>

                <form class="navbar-form navbar-right" role="search">
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="Search Twitter">
                        <span class="input-group-btn">
                            <button type="button" class="btn btn-default">
                                <i class="glyphicon glyphicon-search"></i>
                            </button>
                        </span>
                    </div>
                </form>
            </div><!-- /.navbar-collapse -->
        </div>
    </nav>
    <!--</div></div>-->
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                    <div class="panel panel-default profile">
                    <div class="avatar">
                        <span class="glyphicon glyphicon-camera dziecko icon" aria-hidden="true">   
                        </span>
                    </div>
                    <div class="panel-heading profile-background">
                    </div>
                    <div class="panel-body profile-card">
                        <div class="row">
                            <div class="col-lg-3">

                            </div>
                            <div class="col-lg-offset-1 col-lg-8">
                                <?php
                                echo '<strong><a href="userPage.php?userId='.$_SESSION['loggedUserId'].'" class="link-username fullname">'.$_SESSION['fullname'].'</a></strong>'.
                                     '<p><small><a href="userPage.php?'.$_SESSION['loggedUserId'].'" class="link-username">@'.$_SESSION['username'].'</a></small></p>';
                                ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-3">
                                <small>TWEETS</small>
                                <?php
                                $countTweet = count(Tweet::loadTweetByUserId($conn, $_SESSION['loggedUserId']));
                                echo '<span class="profile-card-stats">'.$countTweet.'</span>';
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="btn-group-vertical pull-right">
                    <a href="messagesPage.php" class="btn btn-default">All</a>
                    <a href="messagesPage.php?who=sender" class="btn btn-default">Send</a>
                    <a href="messagesPage.php?who=recipient" class="btn btn-default">Received</a>
                </div>   
            </div>
            <div class="col-md-6">
                <div class = "panel panel-default panels">
                    <div class = "panel-heading">
                            <div class="inlinegroup">
                                Direct Messages
                                <button type="submit" class="btn btn-primary pull-right" data-toggle="modal" data-target="#myNewMessage">New message</button>
                            </div>
                            
                    </div>
   
                    <div class = "panel-body">
                        <div class="list-group">
                            <?php
                            if($_SERVER['REQUEST_METHOD'] === 'GET')
                            {
                                if(isset($_GET['who']))
                                {
                                    if($_GET['who'] === 'sender')
                                    {
                                        $search = Message::loadAllSendMessages($conn,$_SESSION['loggedUserId']);
                                    }else if($_GET['who'] === 'recipient')
                                    {
                                        $search = Message::loadAllReceivedMessages($conn,$_SESSION['loggedUserId']);
                                    }
                                }else
                                {
                                    $search = Message::loadAllMessagesByUserId($conn,$_SESSION['loggedUserId']);
                                }
                            }  else 
                            {
                                $search = Message::loadAllMessagesByUserId($conn,$_SESSION['loggedUserId']);
                            }
                            foreach($search as $row)
                            {
                                $sender = User::loadUserById($conn, $row->getSenderId());
                                $recipient = User::loadUserById($conn, $row->getRecipientId());
                                echo '<a href="#myMessage" class="list-group-item message" data-toggle="modal" data-id="'.$row->getId().'">'.
                                        '<div class="inline-group">'.
                                        'Sender: <strong>' . $sender->getFullname() . '</strong> &sdot; <small>'.$row->getCreationDate().'</small>'.
                                        '</div>'.
                                        '<p class="list-group-item-text">'.
                                            substr($row->getContent(),0,30)."...".
                                        '</p>'.
                                     '</a>';
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <!-- Modal -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Compose new Tweet</h4>
                </div>
                <div class="modal-body">
                    <form method="POST" action="index.php">
                        <div class="form-group">
                            <textarea name="tweet" maxlength="140" class="form-control" placeholder="What's happening?"></textarea>
                        </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary pull-right">Tweet</button>
                </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    
    <!-- Modal -->
    <div class="modal fade" id="myNewMessage" tabindex="-1" role="dialog" aria-labelledby="myNewMessageLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myNewMessageLabel">New message</h4>
                </div>
                <div class="modal-body">
                    <form method="POST" action="messagesPage.php">
                        <div class="form-group">
                            <label for="recipientLabel">Recipient:</label>
                            <input name="recipient" type="text" class="form-control" id="recipientLabel" placeholder="Recipient">
                        </div>    
                        <div class="form-group">
                                <label for="messageContent">Message content:</label>
                                <textarea name="messageContent" class="form-control" id="messageContent" rows="5"></textarea>
                        </div>
                        <div class="modal-footer">
                            <div class="form-inline pull-right">
                            
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>  
                            <button type="submit" class="btn btn-primary">Send</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    
    <!-- Modal -->
    <div class="modal fade" id="myMessage" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Message</h4>
                    <div id="message-header"></div>  
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <div id="content"></div>
                    </div>
                </div>            
            </div>
        </div>
    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <script>
        $('.my-dropdown').tooltip({
            animation: true,
            trigger: 'hover'
        });
        
        $('.message').on('click', function() {
            var numberMessage = $(this).data("id");
            var messageHeader = $('#message-header');
            var content = $('#content');
            
            $.ajax({
                url: 'messageAPI.php',
                type: 'GET',
                data: {id: numberMessage},
                dataType: 'json'
            })
            .done(function(result) {
                var message = result.message;
                var sender = result.sender;
                var recipient = result.recipient;
                messageHeader.html(
                          '<span>Sender: <strong><a class="link-username" href="userPage.php?userId=' + sender.id + '">' + sender.fullname + '</strong></span><br>' +
                          '<span>Recipient: <strong><a class="link-username" href="userPage.php?userId=' + recipient.id + '">' + recipient.fullname + '</a></strong></span>');
                content.html(message.content);  
            })
            .fail(function(request, status, error){
                console.log(status, error);
            });
        });   
//        $('#myModal').on('shown.bs.modal');
//        $('#myNewMessage').on('shown.bs.modal');
//        $('#myMessage').on('shown.bs.modal');

    </script>    
</body>
</html>

