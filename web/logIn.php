<?php
session_start();
require_once 'library.php';

if(isset($_SESSION['loggedUserId'])){
    header("Location: index.php");
    die();
}

if($_SERVER['REQUEST_METHOD'] === 'POST')
{
    if(isset($_POST['email']) && isset($_POST['password']) &&
       !empty($_POST['email']) && !empty($_POST['password']))
    {   
        $email = $conn->real_escape_string($email);
        $email = htmlspecialchars(trim($_POST['email']));
        $password = trim($_POST['password']);

        $loggedUser = User::logIn($conn, $email, $password);
        if($loggedUser)
        {
            $_SESSION['loggedUserId'] = $loggedUser->getId();
            $_SESSION['fullname'] = $loggedUser->getFullname();
            $_SESSION['username'] = $loggedUser->getUsername();
            if(isset($_SESSION['loginError']))
            {
                unset($_SESSION['loginError']);
            }
            header('Location: index.php');
            die();
        }else {
            $_SESSION['loginError'] = "Failed to login. Incorrect e-mail or password."; 
            header('Location: logIn.php');
            die();
        }
    }else
    {
        $_SESSION['loginError'] = "Failed to login. Fill in all fields.";
        header('Location: logIn.php');
        die();
    }
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Twitter</title>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="../css/fullSlider.css">
</head>
<body>
    <!-- Navigation -->
    <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
        <div class="container">    
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="navcol-1">
                
                <ul class="nav navbar-nav navbar-left">
                    <li><a class="navbar-brand navbar-center"><span class="glyphicon glyphicon-flash logo" aria-hidden="true"></span></a></li>
                    <li><a href="logIn.php">Home</a></li>
                    <li><a href="about.php">About</a></li>
                </ul>   

                <form class="navbar-form navbar-right" role="search">
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="Search Twitter">
                        <span class="input-group-btn">
                            <button type="button" class="btn btn-default">
                                <i class="glyphicon glyphicon-search"></i>
                            </button>
                        </span>
                    </div>
                </form>
            </div> <!-- /.navbar-collapse -->
        </div>  <!-- /.container -->
    </nav>
    
    <!-- Full Page Image Background Carousel Header -->
    <header id="myCarousel" class="carousel slide">
        <!-- Indicators -->
        <ol class="carousel-indicators">
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1"></li>
            <li data-target="#myCarousel" data-slide-to="2"></li>
        </ol>

        <!-- Wrapper for Slides -->
        <div class="carousel-inner">
            <div class="item active">
                <!-- Set the first background image using inline CSS below. -->
                <div class="fill" style="background-image:url('../images/slider3.jpg');"></div>
            </div>
            <div class="item">
                <!-- Set the second background image using inline CSS below. -->
                <div class="fill" style="background-image:url('../images/slider6.jpg');"></div>
            </div>
            <div class="item">
                <!-- Set the third background image using inline CSS below. -->
                <div class="fill" style="background-image:url('../images/slider5.jpg');"></div>
            </div>
        </div>
    </header>
    
    <div class="front-card">
        <div class="container">
        <!-- Marketing Icons Section -->
            <div class="row">
                <div class="col-md-offset-1 col-md-5 welcome">
                    <h1><b>Welcome to Twitter</b></h1>
                    <p>Connect with your friends — and other fascinating people. Get in-the-moment updates on the things that interest you. And watch events unfold, in real time, from every angle.</p>
                </div>
                <div class="col-md-offset-1 col-md-4">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <form method="POST" action="logIn.php">
                                <?php 
                                if(isset($_SESSION['loginError'])){
                                    echo '<div class="alert alert-danger">'.
                                         $_SESSION['loginError'].
                                         '</div>';  
                                }
                                ?>
                                <div class="form-group row">
                                    <div class="col-md-10">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                                            <input type="email" name="email" class="form-control" placeholder="Email">
                                        </div>
                                    </div>    
                                </div>
                                <div class="form-group row">
                                    <div class="col-md-7">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                                            <input type="password" name="password" class="form-control" placeholder="Password">
                                        </div>
                                    </div>  
                                    <div clas="col-md-2">
                                        <button type="submit" class="btn btn-primary">Log in</button>
                                    </div>
                                </div>
                                <div class="checkbox">
                                    <label class="form-check-label">
                                        <input class="form-check-input" type="checkbox"> Remember me
                                    </label>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h5>New to Twitter? Sign up</h5>
                        </div>
                        <div class="panel-body">
                            <form method="POST" action="register.php">
                                <div class="form-group row">
                                    <div class="col-md-12">
                                        <input type="text" name="fullname" class="form-control" placeholder="Full name">
                                    </div>    
                                </div>   
                                <div class="form-group row">    
                                    <div class="col-md-12">
                                        <input type="email" name="email" class="form-control" placeholder="Email">
                                    </div>
                                </div>
                                <div class="form-group row">    
                                    <div class="col-md-12">
                                        <input type="password" name="password" class="form-control" placeholder="Password">
                                    </div>
                                </div>
                                <div clas="col-md-5">
                                    <button type="submit" class="btn btn-warning singUp pull-right">Sign up for Twitter</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div> 
        </div>
    </div>
    
    
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <!-- Script to Activate the Carousel -->
    <script>
        $('.carousel').carousel({
        interval: 5000 //changes the speed
        })
    </script>   
</body>
</html>