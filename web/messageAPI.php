<?php
session_start();
require 'library.php';

header('Content-Type: application/json');

if($_SERVER['REQUEST_METHOD'] === 'GET'){
    $message = [];
    if(isset($_GET['id']) && intval($_GET['id'])>0)
    {
        $message = Message::loadMessageById($conn, $_GET['id']);
        $sender = User::loadUserById($conn, $message->getSenderId());
        $recipient = User::loadUserById($conn, $message->getRecipientId());
        echo json_encode(array("message" => $message, "sender" => $sender, "recipient" => $recipient));
    }

}

?>

